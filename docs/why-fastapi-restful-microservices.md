# RESTFUL 마이크로 서비스를 위해 Flask와 Django 대신 FAST API를 선택한 이유 <sup>[1](#footnote_1)</sup>

## 소개
Django와 Flask는 모두 훌륭한 프레임워크이며, 수년 동안 개발자들은 만들고자 하는 서비스에 따라 두 가지를 혼용하여 사용해 왔다.

서비스가 데이터베이스에 의존하고 간단한 관리자 인터페이스가 필요하며 멋진 웹 GUI가 필요할 때 Django가 유용하며, 놀라운 ORM, 관리자 앱, 템플릿 엔진 덕분에 이 모든 것이 Django에서 즉시 제공된다.

몇 개의 API 엔드포인트를 노출하는 간단한 마이크로서비스가 필요한 경우 Flask가 빛을 발한다. 또한 템플릿이나 ORM이 필요한 경우 외부 패키지가 많기 때문에 Flask는 쉽게 확장할 수 있다.

그러나 RESTful 마이크로서비스의 경우, Flask와 Django 모두 성능과 개발 속도 면에서 기대에 미치지 못했다. 그러던 중 FastAPI를 찾았다.

## 성능
[techempower becnchmark](https://www.techempower.com/benchmarks/#section=test&runid=7464e520-0dc2-473d-bd34-dbdfd7e85911&hw=ph&test=query&l=zijzen-7)에 따르면, FastAPI는 성능 면에서 다른 모든 프레임워크를 능가하며, Flask와 Django는 상위 10위권 내에 들지 못했다.

![](./images/1_uprOURnw5wj-0Kt4HL1YhA.webp)

정말 빠르다!

## 개발 속도
여기서는 다음과 같은 기본 요구 사항을 만족하는 RESTful 마이크로 서비스만 고려한다.

- REST 엔드포인트를 노출
- OpenAPI(Swagger) 문서
- 데이터베이스 연결(Optional)

### Django
Django 패키지에는 REST API가 포함되어 있으므로 [Django-rest-framework](https://www.django-rest-framework.org/)가 최선의 선택이다. 수많은 기능이 포함되어 있지만 Open API 생성은 그 기능 중 하나가 아니다. 하지만 REST API에 대한 자체 문서가 함께 제공된다. Open API 문서의 경우, [drf-yasg](https://drf-yasg.readthedocs.io/en/stable/) 또는 [django-rest-swagger](https://django-rest-swagger.readthedocs.io/en/latest/) 패키지를 사용할 수 있다. 이 중 어떤 것을 사용하든 문서를 확장하는 것은 상당히 번거로운 일이며, 때로는 .yaml 파일을 대신 사용해야 하는 경우도 있다. 관계형 데이터베이스나 데이터베이스를 전혀 사용하지 않으려는 경우에도 ORM을 포함하여야 한다.

### Flask
Flask로 REST 엔드포인트를 생성하는 데는 외부 패키지가 필요하지 않지만, Open API 문서의 경우 [Flask-restplus](https://flask-restplus.readthedocs.io/en/stable/)와 같은 외부 패키지를 사용할 수 있지만, 일반적으로 오래된 버전의 Open API 문서를 생성한다.

### FastAPI
FastAPI와 Open API를 사용하여 REST API 엔드포인트를 만드는 데 5분도 채 걸리지 않는다.

```python
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel


class User(BaseModel):
    first_name: str
    last_name: str = None
    age: int


app = FastAPI()


@app.post("/user/", response_model=User)
async def create_user(user: User):
    result = await some_library(user)
    return result

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
```

위의 코드는 Open API 버전 3 문서(기본값)를 사용하여 `user`를 생성할 수 있는 API 엔드포인트를 노출한다.

![](./images/1_kd3CiRYZWgUEXvWYGsSypg.webp)

ReDoc 문서는 보너스이다.

![](./images/1_hR8doSsPKdJwLQ7qOxokWg.webp)

커스텀 문서로 변경하고 유효성 검사를 추가하는 것은 매우 간단하며 [여기](https://pydantic-docs.helpmanual.io/usage/schema/) 문서에 설명되어 있다.

## 끝으로
아래 코드를 보았나요?

```python
@app.post("/user/", response_model=User)
async def create_user(user: User):
    result = await some_library(user)
    return result
```

`create_user` 함수는 실제로 비동기성을 지원하기 때문에 비동기적으로 실행된다. 이제 Django 3.0에서는 ORM(동기화)을 사용해야 하는 경우 `SynchronousOnlyOperation` 예외가 발생할 때까지 이 작업을 수행할 수 있다. 그러나 FastAPI에서는 [`database`](https://github.com/encode/databases) 또는 [`gino`](https://python-gino.org/)를 사용하여 달성할 수 없다.

`sync`와 `async` 데이터베이스로 작업하는 방법을 알고 싶다면 이 [**튜토리얼**](https://medium.com/@ahmed.nafies/fastapi-with-sqlalchemy-postgresql-and-alembic-and-of-course-docker-f2b7411ee396) 가이드를 참조하세요.


<a name="footnote_1">1</a>: 이 페이지는 [Why did we choose FAST API over Flask and Django for our RESTFUL Micro-services](https://ahmed-nafies.medium.com/why-did-we-choose-fast-api-over-flask-and-django-for-our-restful-micro-services-77589534c036)을 편역한 것임.
